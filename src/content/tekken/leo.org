#+SETUPFILE: ../../templates/rec_template.org
#+TITLE: Leo

* Combos

Staples as of now
(BBP is b1+21+2qcf2)

df2 4 f2 b1,1+2~D BOK12 T! BBP

uf21 iws3 b1,1+2~D BOK12 T! BBP

[We need a low parry one here]

BOK2 f3 4 b1,1+2~D BOK12 T! BBP

CH KNK4~df ws3 4 b1,1+2~D BOK12 T! f4 KNK~df qcf24

CH df2+3 ws3 4 b1,1+2~D BOK12 T! BBP

CH 4 BOK1+2 d3+4, they can toe kick the stomp, but you can then whiff punish to kill instead

CH d2 4 f2 b1,1+2~D BOK12 T! BBP
Wall Ender
bok1+2 d3+4
b14 KNK1+2
b14 KNK~df ws1+2 > LTG


launch, 1, b1,1+2~d, BOK1,2, T! works at all angles, would recommend  (edited)
Jump
