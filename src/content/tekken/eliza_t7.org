#+SETUPFILE: ../../templates/rec_template.org
#+TITLE: Eliza Combos and Tech

* Punishes
#+BEGIN_EXAMPLE
--- Standing ---
- i10
+> 1,super
+> 1,dp+2
+> d+1,super
+> d+1,dp+2
- i11
+> b+2,3
- i13
+> df+1,2
+> 1+2
+> 4
- i14
+> 2,2,super
+> 2,2,dp+2
- i15
+> b+4,dp+1
+> b+4,qcb+1
- i16
+> df+2,3

--- While Standing ---
- i14
+> ws1,dp+1
- i15
+> ws3
#+END_EXAMPLE
* Combo
** f, f+3+4
iWR3, b+1,2,4

ff34, dk4, dk4, dk4, 1, 1, ff3, 4

f,f+3,4 > forward jump qcb+4 > forward jump qcb+4 > forward jump qcb+4 > dash 1 > MG (f+4) 1,2,1+2 [56 DMG

*** Easy Mode
jDK, dash 1, dash 1, dash 1, (dash 1), f,f+3,4
Note: cancel into dp2 for wallsplat


** B4 xx dp1
b4 xx dp1 > b4 xx qcb4 > air dk4 > air dk4 >ff1 > ff1 > ff3,4 > MG 3,4
** CH D+4
WS4, B4xxQCB1, F+4,1, 1, f+4 1,1, 1+2

WS2, d3dp1,b4qcb1 f 121+2

** QCB 3+4
MG 1, TK DK4, aDK4, aDK4, dash 1, dash 1, MG 1,1,1+2
** QCF 1+2
QCF1+2 MG34 
** W!
b4 xx QCB 4, b4 xx QCB2
b4 xx QCB 4, b4 xx QCB1
** 3+4
idk4, idk4, 4, mg1, 1, mg1 2 1+2
** 1dp2 super
1 dp2 super qcb1 wr3 b4 dp2
** Low Parry
d/f+1, 4, fff+3, b+1,2,4

qcb4 b23 b32 mg1,2,1+2
* Tech
SSR d3xxdp1 for resplat after QCB2 at the wall
** Jump Over Setups
W! 2,2 xx qcb4
W! 1,2 jump over d+1, qcb+4
** Unblockable Setups
(launcher) d/f+1, b+2, 3, 1, MG1, b+4 xx qcb+1 S!. MG 1+2 is they roll, MG, n, d+3 xx dp1, b+1, 2, 4 if they stay down

ff 3 4 mg 1 unblockable
