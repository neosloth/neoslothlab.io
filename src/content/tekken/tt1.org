#+SETUPFILE: ../../templates/rec_template.org
#+TITLE: Old Tekken Stuff
* TT1
** Devil
Literally just HS and df1,2

*** Combos
df1,2 4

* T5 DR
** Devil Jin

uf+4 db+2

f,n,hcf+1+2 db2

EWGF EWGF 1+4 df1,2~f,f CD4

EWGF ff3,1 df1,2~f,f CD4

EWGF ff3,1 bf2,1,4

CD4 df1,2 CD4

At the wall: CD4 1+4 d+3+4 
** Kazuya

FF+3 D+4

EWGF DEWGF 1 b+2,4,1

EWGF DEWGF 1 1 1 CD+4,1

ws1,2 f+3,1,4 1 DEWGF

df1,4 1 1 1 dewf

WS2 tgf,3

** Julia
df2,3 f+3~1 df4 ff1 ff1 d,df+1,2

uf+4,3 ff+1 d,df+1,2
* T6
EWGF f,f,3,1~f,f 2 df+1+2 B! f,f,3,1~f,f EWGF

u4~u FLY3 B! ssl 1+4 b,f,2,1,2

CH b4 df1,2 cd3 d3
* TTT2 DVJ/HWO
#+begin_export html
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/awmr4EyGfEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
#+end_export

EWGF (EWGF) ff3,1~ff 2 df1+2 B! HWO RFF f+3 uf3,4,3 DVJ f+3+4

HWO df2 u3 LFS uf3+4 4,4,3~5 B! DVJ 4~3 HWO df+3+4

DVJ ws2 DEWGF 2 df1+2~5 B! HWO RF f+3 uf3,4,3 DVJ f3+4

HWO CH b2 4,4,3~5 B! 

HWO JFSR RFF 3~4 dash f+1,2,4,2 RFF f,f+3 B! dash RFF 3~4~f,b+4

HWO JFSR 3~4 dash 2,4,2 RFF f,f+3+3 B!

HWO W! u+3 4,4,3 
* TTT2 LEO/HWO
 df2 f2 b14~df~uf1 b2 1+2 TA! uf343 f1+2 - 90dmg 
