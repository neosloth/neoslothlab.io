#+SETUPFILE: ../../templates/rec_template.org
#+TITLE:Katarina Combos and Tech
* Combos
** uf/4 ch df4
uf4,4 d/f+1, 4,4,4f HAR df2,3 S! d2f HAR 4 (67)
uf4,4 dash 4 d2f db ws4 d2f HAR df2,3 S! d2f HAR4 (72)

** WS2
b1,f HAR 1 d1 2,2 S! d2,f HAR 4

** 444
444~f HAR db ws4 d2f HAR df23 S! d2f HAR4
444~f HAR b+1,F HAR df2,3 S! d2f HAR4
** f,f24

f,f24 S! d2f ws4 d2f HAR df2,3

** Wall
df1,1~f HAR 4
1, df1 33333
1, df1 ub4

** Off axis
uf44 ff4 444~har
