#+TITLE: Minimal setup org mode website and blog

Testing page don't worry about it

* TODO Source 
#+BEGIN_SRC emacs-lisp
(defun neo-postamble (plist)
  "<footer>
    <p>This site was generated using <a href=\"https://orgmode.org/\">org mode</a></p>
  </footer>")

(setq neo-site-head-extra "<link rel='stylesheet' type='text/css' href='../css/style.css' />")

;; Custom blog sitemap
;; Taken from https://www.evenchick.com/blog/blogging-with-org-mode.html
(defun neo-site-format-entry (entry style project)
  (format "[[file:%s][%s]] --- %s"
          entry
          (org-publish-find-title entry project)
          (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))))

(setq org-publish-project-alist
      `(("org-content"
         :author "neosloth"
         ;; Location of org files
         :base-directory "~/Sync/Notes/website/content/"
         :base-extension "org"
         :publishing-directory "~/Sync/publish/"
         :auto-sitemap nil
         :html-postamble neo-postamble
         :html-html5-fancy t
         :htmlized-source t
         :recursive t
         :publishing-function org-html-publish-to-html)

        ("org-blog"
         :author "neosloth"
         ;; Location of org files
         :base-directory "~/Sync/Notes/website/blog/"
         :base-extension "org"
         :publishing-directory "~/Sync/publish/blog/"
         :html-postamble neo-postamble
         :html-html5-fancy t
         ;; Content has css links in the template
         :html-head-extra ,neo-site-head-extra
         :html-link-home "../index.html"
         :html-link-up "./index.html"
         :htmlized-source t
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-title "Articles"
         :sitemap-style list
         :sitemap-sort-files anti-chronologically
         :sitemap-format-entry neo-site-format-entry

         :recursive t
         :publishing-function org-html-publish-to-html)

        ("org-static"
         :base-directory "~/Sync/Notes/website/static/"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|dec\\|m4a"
         :publishing-directory "~/Sync/publish/"
         :recursive t
         :publishing-function org-publish-attachment)


        ("website" :components ("org-content" "org-static" "org-blog"))))

#+END_SRC
